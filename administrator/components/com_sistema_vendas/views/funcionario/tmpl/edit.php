<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Sistema_vendas
 * @author     Paulo Henrique Flausino Ferreira <pauloflausino@gmail.com>
 * @copyright  2020 Paulo Henrique Flausino Ferreira
 * @license    GNU General Public License versão 2 ou posterior; consulte o arquivo License. txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;


HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/html');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');
HTMLHelper::_('behavior.keepalive');

// Import CSS
$document = Factory::getDocument();
$document->addStyleSheet(Uri::root() . 'media/com_sistema_vendas/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'funcionario.cancel') {
			Joomla.submitform(task, document.getElementById('funcionario-form'));
		}
		else {
			
			if (task != 'funcionario.cancel' && document.formvalidator.isValid(document.id('funcionario-form'))) {
				
				Joomla.submitform(task, document.getElementById('funcionario-form'));
			}
			else {
				alert('<?php echo $this->escape(Text::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_sistema_vendas&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="funcionario-form" class="form-validate form-horizontal">

	
	<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
	<?php echo $this->form->renderField('created_by'); ?>
	<?php echo $this->form->renderField('modified_by'); ?>
	<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'funcionario')); ?>
	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'funcionario', JText::_('COM_SISTEMA_VENDAS_TAB_FUNCIONARIO', true)); ?>
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset class="adminform">
				<legend><?php echo JText::_('COM_SISTEMA_VENDAS_FIELDSET_FUNCIONARIO'); ?></legend>
				<?php echo $this->form->renderField('nome'); ?>
				<?php echo $this->form->renderField('email'); ?>
				<?php echo $this->form->renderField('telefone'); ?>
				<?php echo $this->form->renderField('cpf'); ?>
				<?php echo $this->form->renderField('rg'); ?>
				<?php echo $this->form->renderField('data_nascimento'); ?>
				<?php echo $this->form->renderField('ctps'); ?>
				<?php echo $this->form->renderField('cargo'); ?>
				<?php echo $this->form->renderField('data_ingresso'); ?>
				<?php echo $this->form->renderField('data_demissao'); ?>
				<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
				<?php endif; ?>
			</fieldset>
		</div>
	</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	
	<?php echo JHtml::_('bootstrap.endTabSet'); ?>

	<input type="hidden" name="task" value=""/>
	<?php echo JHtml::_('form.token'); ?>

</form>
