<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Sistema_vendas
 * @author     Paulo Henrique Flausino Ferreira <pauloflausino@gmail.com>
 * @copyright  2020 Paulo Henrique Flausino Ferreira
 * @license    GNU General Public License versão 2 ou posterior; consulte o arquivo License. txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Funcionario controller class.
 *
 * @since  1.6
 */
class Sistema_vendasControllerFuncionario extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'funcionarios';
		parent::__construct();
	}
}
