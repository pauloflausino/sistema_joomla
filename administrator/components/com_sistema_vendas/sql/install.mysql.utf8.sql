CREATE TABLE IF NOT EXISTS `#__funcionarios` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`nome` VARCHAR(255)  NOT NULL ,
`email` VARCHAR(255)  NOT NULL ,
`telefone` VARCHAR(255)  NOT NULL ,
`cpf` VARCHAR(255)  NOT NULL ,
`rg` VARCHAR(255)  NOT NULL ,
`data_nascimento` DATETIME NOT NULL ,
`ctps` VARCHAR(255)  NOT NULL ,
`cargo` VARCHAR(255)  NOT NULL ,
`data_ingresso` DATETIME NOT NULL ,
`data_demissao` DATETIME NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;


INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `field_mappings`, `content_history_options`)
SELECT * FROM ( SELECT 'Funcionário','com_sistema_vendas.funcionario','{"special":{"dbtable":"#__funcionarios","key":"id","type":"Funcionario","prefix":"Sistema_vendasTable"}}', CASE 
                                WHEN 'field_mappings' is null THEN ''
                                ELSE ''
                                END as field_mappings, '{"formFile":"administrator\/components\/com_sistema_vendas\/models\/forms\/funcionario.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_sistema_vendas.funcionario')
) LIMIT 1;
