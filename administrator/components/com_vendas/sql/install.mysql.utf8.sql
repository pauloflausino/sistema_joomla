CREATE TABLE IF NOT EXISTS `#__vendas_clientes` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`razao_social_cliente` VARCHAR(255)  NOT NULL ,
`cliente_email` VARCHAR(255)  NOT NULL ,
`cliente_telefone` VARCHAR(255)  NOT NULL ,
`cliente_cnpj` VARCHAR(255)  NOT NULL ,
`data_proposta` DATETIME NOT NULL ,
`descricao_proposta` TEXT NOT NULL ,
`status_proposta` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;


INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `field_mappings`, `content_history_options`)
SELECT * FROM ( SELECT 'Cliente','com_vendas.cliente','{"special":{"dbtable":"#__vendas_clientes","key":"id","type":"Cliente","prefix":"VendasTable"}}', CASE 
                                WHEN 'field_mappings' is null THEN ''
                                ELSE ''
                                END as field_mappings, '{"formFile":"administrator\/components\/com_vendas\/models\/forms\/cliente.xml", "hideFields":["checked_out","checked_out_time","params","language" ,"descricao_proposta"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_vendas.cliente')
) LIMIT 1;
