<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Sistema_vendas
 * @author     Paulo Henrique Flausino Ferreira <pauloflausino@gmail.com>
 * @copyright  2020 Paulo Henrique Flausino Ferreira
 * @license    GNU General Public License versão 2 ou posterior; consulte o arquivo License. txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

HTMLHelper::_('behavior.keepalive');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');

// Load admin language file
$lang = Factory::getLanguage();
$lang->load('com_sistema_vendas', JPATH_SITE);
$doc = Factory::getDocument();
$doc->addScript(Uri::base() . '/media/com_sistema_vendas/js/form.js');

$user    = Factory::getUser();
$canEdit = Sistema_vendasHelpersSistema_vendas::canUserEdit($this->item, $user);


?>

<div class="funcionario-edit front-end-edit">
	<?php if (!$canEdit) : ?>
		<h3>
			<?php throw new Exception(Text::_('COM_SISTEMA_VENDAS_ERROR_MESSAGE_NOT_AUTHORISED'), 403); ?>
		</h3>
	<?php else : ?>
		<?php if (!empty($this->item->id)): ?>
			<h1><?php echo Text::sprintf('COM_SISTEMA_VENDAS_EDIT_ITEM_TITLE', $this->item->id); ?></h1>
		<?php else: ?>
			<h1><?php echo Text::_('COM_SISTEMA_VENDAS_ADD_ITEM_TITLE'); ?></h1>
		<?php endif; ?>

		<form id="form-funcionario"
			  action="<?php echo Route::_('index.php?option=com_sistema_vendas&task=funcionario.save'); ?>"
			  method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
			
	<input type="hidden" name="jform[id]" value="<?php echo isset($this->item->id) ? $this->item->id : ''; ?>" />

				<?php echo $this->form->getInput('created_by'); ?>
				<?php echo $this->form->getInput('modified_by'); ?>
	<?php echo $this->form->renderField('nome'); ?>

	<?php echo $this->form->renderField('email'); ?>

	<?php echo $this->form->renderField('telefone'); ?>

	<?php echo $this->form->renderField('cpf'); ?>

	<?php echo $this->form->renderField('rg'); ?>

	<?php echo $this->form->renderField('data_nascimento'); ?>

	<?php echo $this->form->renderField('ctps'); ?>

	<?php echo $this->form->renderField('cargo'); ?>

	<?php echo $this->form->renderField('data_ingresso'); ?>

	<?php echo $this->form->renderField('data_demissao'); ?>

			<div class="control-group">
				<div class="controls">

					<?php if ($this->canSave): ?>
						<button type="submit" class="validate btn btn-primary">
							<?php echo Text::_('JSUBMIT'); ?>
						</button>
					<?php endif; ?>
					<a class="btn"
					   href="<?php echo Route::_('index.php?option=com_sistema_vendas&task=funcionarioform.cancel'); ?>"
					   title="<?php echo Text::_('JCANCEL'); ?>">
						<?php echo Text::_('JCANCEL'); ?>
					</a>
				</div>
			</div>

			<input type="hidden" name="option" value="com_sistema_vendas"/>
			<input type="hidden" name="task"
				   value="funcionarioform.save"/>
			<?php echo HTMLHelper::_('form.token'); ?>
		</form>
	<?php endif; ?>
</div>
