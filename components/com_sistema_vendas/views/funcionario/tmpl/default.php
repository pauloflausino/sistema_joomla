<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Sistema_vendas
 * @author     Paulo Henrique Flausino Ferreira <pauloflausino@gmail.com>
 * @copyright  2020 Paulo Henrique Flausino Ferreira
 * @license    GNU General Public License versão 2 ou posterior; consulte o arquivo License. txt
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_sistema_vendas');

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_sistema_vendas'))
{
	$canEdit = JFactory::getUser()->id == $this->item->created_by;


}
?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_SISTEMA_VENDAS_FORM_LBL_FUNCIONARIO_NOME'); ?></th>
			<td><?php echo $this->item->nome; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_SISTEMA_VENDAS_FORM_LBL_FUNCIONARIO_EMAIL'); ?></th>
			<td><?php echo $this->item->email; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_SISTEMA_VENDAS_FORM_LBL_FUNCIONARIO_TELEFONE'); ?></th>
			<td><?php echo $this->item->telefone; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_SISTEMA_VENDAS_FORM_LBL_FUNCIONARIO_CPF'); ?></th>
			<td><?php echo $this->item->cpf; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_SISTEMA_VENDAS_FORM_LBL_FUNCIONARIO_RG'); ?></th>
			<td><?php echo $this->item->rg; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_SISTEMA_VENDAS_FORM_LBL_FUNCIONARIO_DATA_NASCIMENTO'); ?></th>
			<td><?php echo $this->item->data_nascimento; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_SISTEMA_VENDAS_FORM_LBL_FUNCIONARIO_CTPS'); ?></th>
			<td><?php echo $this->item->ctps; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_SISTEMA_VENDAS_FORM_LBL_FUNCIONARIO_CARGO'); ?></th>
			<td><?php echo $this->item->cargo; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_SISTEMA_VENDAS_FORM_LBL_FUNCIONARIO_DATA_INGRESSO'); ?></th>
			<td><?php echo $this->item->data_ingresso; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_SISTEMA_VENDAS_FORM_LBL_FUNCIONARIO_DATA_DEMISSAO'); ?></th>
			<td><?php echo $this->item->data_demissao; ?></td>
		</tr>

	</table>

</div>

<?php if($canEdit): ?>

	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_sistema_vendas&task=funcionario.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_SISTEMA_VENDAS_EDIT_ITEM"); ?></a>

<?php endif; ?>

<?php if (JFactory::getUser()->authorise('core.delete','com_sistema_vendas.funcionario.'.$this->item->id)) : ?>

	<a class="btn btn-danger" href="#deleteModal" role="button" data-toggle="modal">
		<?php echo JText::_("COM_SISTEMA_VENDAS_DELETE_ITEM"); ?>
	</a>

	<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><?php echo JText::_('COM_SISTEMA_VENDAS_DELETE_ITEM'); ?></h3>
		</div>
		<div class="modal-body">
			<p><?php echo JText::sprintf('COM_SISTEMA_VENDAS_DELETE_CONFIRM', $this->item->id); ?></p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Close</button>
			<a href="<?php echo JRoute::_('index.php?option=com_sistema_vendas&task=funcionario.remove&id=' . $this->item->id, false, 2); ?>" class="btn btn-danger">
				<?php echo JText::_('COM_SISTEMA_VENDAS_DELETE_ITEM'); ?>
			</a>
		</div>
	</div>

<?php endif; ?>