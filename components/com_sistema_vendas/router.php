<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Sistema_vendas
 * @author     Paulo Henrique Flausino Ferreira <pauloflausino@gmail.com>
 * @copyright  2020 Paulo Henrique Flausino Ferreira
 * @license    GNU General Public License versão 2 ou posterior; consulte o arquivo License. txt
 */
// No direct access
defined('_JEXEC') or die;

use Joomla\CMS\Component\Router\RouterViewConfiguration;
use Joomla\CMS\Component\Router\RouterView;
use Joomla\CMS\Component\Router\Rules\StandardRules;
use Joomla\CMS\Component\Router\Rules\NomenuRules;
use Joomla\CMS\Component\Router\Rules\MenuRules;
use Joomla\CMS\Factory;
use Joomla\CMS\Categories\Categories;

/**
 * Class Sistema_vendasRouter
 *
 */
class Sistema_vendasRouter extends RouterView
{
	private $noIDs;
	public function __construct($app = null, $menu = null)
	{
		$params = Factory::getApplication()->getParams('com_sistema_vendas');
		$this->noIDs = (bool) $params->get('sef_ids');
		
		$funcionarios = new RouterViewConfiguration('funcionarios');
		$this->registerView($funcionarios);
			$funcionario = new RouterViewConfiguration('funcionario');
			$funcionario->setKey('id')->setParent($funcionarios);
			$this->registerView($funcionario);
			$funcionarioform = new RouterViewConfiguration('funcionarioform');
			$funcionarioform->setKey('id');
			$this->registerView($funcionarioform);

		parent::__construct($app, $menu);

		$this->attachRule(new MenuRules($this));

		if ($params->get('sef_advanced', 0))
		{
			$this->attachRule(new StandardRules($this));
			$this->attachRule(new NomenuRules($this));
		}
		else
		{
			JLoader::register('Sistema_vendasRulesLegacy', __DIR__ . '/helpers/legacyrouter.php');
			JLoader::register('Sistema_vendasHelpersSistema_vendas', __DIR__ . '/helpers/sistema_vendas.php');
			$this->attachRule(new Sistema_vendasRulesLegacy($this));
		}
	}


	
		/**
		 * Method to get the segment(s) for an funcionario
		 *
		 * @param   string  $id     ID of the funcionario to retrieve the segments for
		 * @param   array   $query  The request that is built right now
		 *
		 * @return  array|string  The segments of this item
		 */
		public function getFuncionarioSegment($id, $query)
		{
			return array((int) $id => $id);
		}
			/**
			 * Method to get the segment(s) for an funcionarioform
			 *
			 * @param   string  $id     ID of the funcionarioform to retrieve the segments for
			 * @param   array   $query  The request that is built right now
			 *
			 * @return  array|string  The segments of this item
			 */
			public function getFuncionarioformSegment($id, $query)
			{
				return $this->getFuncionarioSegment($id, $query);
			}

	
		/**
		 * Method to get the segment(s) for an funcionario
		 *
		 * @param   string  $segment  Segment of the funcionario to retrieve the ID for
		 * @param   array   $query    The request that is parsed right now
		 *
		 * @return  mixed   The id of this item or false
		 */
		public function getFuncionarioId($segment, $query)
		{
			return (int) $segment;
		}
			/**
			 * Method to get the segment(s) for an funcionarioform
			 *
			 * @param   string  $segment  Segment of the funcionarioform to retrieve the ID for
			 * @param   array   $query    The request that is parsed right now
			 *
			 * @return  mixed   The id of this item or false
			 */
			public function getFuncionarioformId($segment, $query)
			{
				return $this->getFuncionarioId($segment, $query);
			}
}
