<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Sistema_vendas
 * @author     Paulo Henrique Flausino Ferreira <pauloflausino@gmail.com>
 * @copyright  2020 Paulo Henrique Flausino Ferreira
 * @license    GNU General Public License versão 2 ou posterior; consulte o arquivo License. txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Controller\BaseController;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Sistema_vendas', JPATH_COMPONENT);
JLoader::register('Sistema_vendasController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = BaseController::getInstance('Sistema_vendas');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
