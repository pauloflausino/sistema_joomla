<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Vendas
 * @author     Paulo Henrique Flausino Ferreira <pauloflausino@gmail.com>
 * @copyright  2020 Paulo Henrique Flausino Ferreira
 * @license    GNU General Public License versão 2 ou posterior; consulte o arquivo License. txt
 */
// No direct access
defined('_JEXEC') or die;

use Joomla\CMS\Component\Router\RouterViewConfiguration;
use Joomla\CMS\Component\Router\RouterView;
use Joomla\CMS\Component\Router\Rules\StandardRules;
use Joomla\CMS\Component\Router\Rules\NomenuRules;
use Joomla\CMS\Component\Router\Rules\MenuRules;
use Joomla\CMS\Factory;
use Joomla\CMS\Categories\Categories;

/**
 * Class VendasRouter
 *
 */
class VendasRouter extends RouterView
{
	private $noIDs;
	public function __construct($app = null, $menu = null)
	{
		$params = Factory::getApplication()->getParams('com_vendas');
		$this->noIDs = (bool) $params->get('sef_ids');
		
		$clientes = new RouterViewConfiguration('clientes');
		$this->registerView($clientes);
			$cliente = new RouterViewConfiguration('cliente');
			$cliente->setKey('id')->setParent($clientes);
			$this->registerView($cliente);
			$clienteform = new RouterViewConfiguration('clienteform');
			$clienteform->setKey('id');
			$this->registerView($clienteform);

		parent::__construct($app, $menu);

		$this->attachRule(new MenuRules($this));

		if ($params->get('sef_advanced', 0))
		{
			$this->attachRule(new StandardRules($this));
			$this->attachRule(new NomenuRules($this));
		}
		else
		{
			JLoader::register('VendasRulesLegacy', __DIR__ . '/helpers/legacyrouter.php');
			JLoader::register('VendasHelpersVendas', __DIR__ . '/helpers/vendas.php');
			$this->attachRule(new VendasRulesLegacy($this));
		}
	}


	
		/**
		 * Method to get the segment(s) for an cliente
		 *
		 * @param   string  $id     ID of the cliente to retrieve the segments for
		 * @param   array   $query  The request that is built right now
		 *
		 * @return  array|string  The segments of this item
		 */
		public function getClienteSegment($id, $query)
		{
			return array((int) $id => $id);
		}
			/**
			 * Method to get the segment(s) for an clienteform
			 *
			 * @param   string  $id     ID of the clienteform to retrieve the segments for
			 * @param   array   $query  The request that is built right now
			 *
			 * @return  array|string  The segments of this item
			 */
			public function getClienteformSegment($id, $query)
			{
				return $this->getClienteSegment($id, $query);
			}

	
		/**
		 * Method to get the segment(s) for an cliente
		 *
		 * @param   string  $segment  Segment of the cliente to retrieve the ID for
		 * @param   array   $query    The request that is parsed right now
		 *
		 * @return  mixed   The id of this item or false
		 */
		public function getClienteId($segment, $query)
		{
			return (int) $segment;
		}
			/**
			 * Method to get the segment(s) for an clienteform
			 *
			 * @param   string  $segment  Segment of the clienteform to retrieve the ID for
			 * @param   array   $query    The request that is parsed right now
			 *
			 * @return  mixed   The id of this item or false
			 */
			public function getClienteformId($segment, $query)
			{
				return $this->getClienteId($segment, $query);
			}
}
