<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Vendas
 * @author     Paulo Henrique Flausino Ferreira <pauloflausino@gmail.com>
 * @copyright  2020 Paulo Henrique Flausino Ferreira
 * @license    GNU General Public License versão 2 ou posterior; consulte o arquivo License. txt
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_vendas');

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_vendas'))
{
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
$canEdit = true;
?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_VENDAS_FORM_LBL_CLIENTE_RAZAO_SOCIAL_CLIENTE'); ?></th>
			<td><?php echo $this->item->razao_social_cliente; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_VENDAS_FORM_LBL_CLIENTE_CLIENTE_EMAIL'); ?></th>
			<td><?php echo $this->item->cliente_email; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_VENDAS_FORM_LBL_CLIENTE_CLIENTE_TELEFONE'); ?></th>
			<td><?php echo $this->item->cliente_telefone; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_VENDAS_FORM_LBL_CLIENTE_CLIENTE_CNPJ'); ?></th>
			<td><?php echo $this->item->cliente_cnpj; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_VENDAS_FORM_LBL_CLIENTE_DATA_PROPOSTA'); ?></th>
			<td><?php echo $this->item->data_proposta; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_VENDAS_FORM_LBL_CLIENTE_DESCRICAO_PROPOSTA'); ?></th>
			<td><?php echo nl2br($this->item->descricao_proposta); ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_VENDAS_FORM_LBL_CLIENTE_STATUS_PROPOSTA'); ?></th>
			<td><?php echo $this->item->status_proposta; ?></td>
		</tr>

	</table>

</div>

<?php if($canEdit): ?>

	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_vendas&task=cliente.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_VENDAS_EDIT_ITEM"); ?></a>

<?php endif; ?>

<?php if (JFactory::getUser()->authorise('core.delete','com_vendas.cliente.'.$this->item->id)) : ?>

	<a class="btn btn-danger" href="#deleteModal" role="button" data-toggle="modal">
		<?php echo JText::_("COM_VENDAS_DELETE_ITEM"); ?>
	</a>

	<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><?php echo JText::_('COM_VENDAS_DELETE_ITEM'); ?></h3>
		</div>
		<div class="modal-body">
			<p><?php echo JText::sprintf('COM_VENDAS_DELETE_CONFIRM', $this->item->id); ?></p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Close</button>
			<a href="<?php echo JRoute::_('index.php?option=com_vendas&task=cliente.remove&id=' . $this->item->id, false, 2); ?>" class="btn btn-danger">
				<?php echo JText::_('COM_VENDAS_DELETE_ITEM'); ?>
			</a>
		</div>
	</div>

<?php endif; ?>